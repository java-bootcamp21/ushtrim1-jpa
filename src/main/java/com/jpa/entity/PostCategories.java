package com.jpa.entity;

import java.time.LocalDateTime;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;

@Entity
public class PostCategories extends BaseEntity<Integer>{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "post_id",insertable=false, updatable=false)
	private Integer postId;
	
	@Column(name = "category_id",insertable=false, updatable=false)
	private Integer categoryId;
	private LocalDateTime dateCreated;
	private LocalDateTime dateModified;
	
	@ManyToOne
	@JoinColumn(name="post_id",referencedColumnName = "id")
	private Posts posts;
	
	@ManyToOne
	@JoinColumn(name = "category_id", referencedColumnName = "id")
	private Categories categories;
	
	public Categories getCategories() {
		return categories;
	}


	public void setCategories(Categories categories) {
		this.categories = categories;
	}


	public Posts getPosts() {
		return posts;
	}


	public void setPosts(Posts posts) {
		this.posts = posts;
	}


	public PostCategories() {
		super();
	}
	
	
	public PostCategories(Integer postId, Integer categoryId, LocalDateTime dateCreated, LocalDateTime dateModified) {
		super();
		this.postId = postId;
		this.categoryId = categoryId;
		this.dateCreated = dateCreated;
		this.dateModified = dateModified;
	}


	public Integer getPostId() {
		return postId;
	}


	public void setPostId(Integer postId) {
		this.postId = postId;
	}


	public Integer getCategoryId() {
		return categoryId;
	}


	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}


	public LocalDateTime getDateCreated() {
		return dateCreated;
	}


	public void setDateCreated(LocalDateTime dateCreated) {
		this.dateCreated = dateCreated;
	}


	public LocalDateTime getDateModified() {
		return dateModified;
	}


	public void setDateModified(LocalDateTime dateModified) {
		this.dateModified = dateModified;
	}


	public void setId(Integer id) {
		this.id = id;
	}



	@Override
	public String toString() {
		return "PostCategories [id=" + id + ", postId=" + postId + ", categoryId=" + categoryId + ", dateCreated="
				+ dateCreated + ", dateModified=" + dateModified + "]";
	}


	@Override
	public Integer getId() {
		return id;
	}
}
