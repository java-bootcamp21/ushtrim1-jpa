package com.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.jpa.entity.Posts;
import com.jpa.entity.Users;

import java.util.List;
import com.jpa.entity.PostCategories;



@Repository
public interface PostsRepository extends JpaRepository<Posts, Integer>{

	List<Posts> findPostsByUsers(Users users);
//	List<Posts> findByPostCategories(List<PostCategories> postCategories);

	
	List<Posts> findAllByUsers_id(Integer userId);
	
	
	List<Posts> findAllByUsers_username(String username);
}
