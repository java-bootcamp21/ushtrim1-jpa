package com.jpa;


import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.jpa.entity.Categories;
import com.jpa.entity.PostCategories;
import com.jpa.entity.Posts;
import com.jpa.entity.Users;
import com.jpa.repository.CategoryRepository;
import com.jpa.repository.PostCategoriesRepository;
import com.jpa.repository.PostsRepository;
import com.jpa.repository.UsersRepository;
import com.jpa.service.BlogService;
import com.jpa.service.UserService;

@SpringBootApplication
public class Ushtrim1JpaApplication implements CommandLineRunner {

	Logger logger = LoggerFactory.getLogger(Ushtrim1JpaApplication.class);
	
	private final BlogService blogService;
	private final UserService userService;
	private final UsersRepository usersRepository;
	private final CategoryRepository categoryRepository;
	private final PostsRepository postsRepository;
	private final PostCategoriesRepository postCategoriesRepository;
	
	


	public Ushtrim1JpaApplication(BlogService blogService, UserService userService, UsersRepository usersRepository,
			CategoryRepository categoryRepository, PostsRepository postsRepository,
			PostCategoriesRepository postCategoriesRepository) {
		super();
		this.blogService = blogService;
		this.userService = userService;
		this.usersRepository = usersRepository;
		this.categoryRepository = categoryRepository;
		this.postsRepository = postsRepository;
		this.postCategoriesRepository = postCategoriesRepository;
	}

	public static void main(String[] args) {
		SpringApplication.run(Ushtrim1JpaApplication.class, args);
		
	}

	@Override
	public void run(String... args) throws Exception {
		
		usersRepository.deleteAll();
		postCategoriesRepository.deleteAll();
		postsRepository.deleteAll();
		categoryRepository.deleteAll();
		
		
		Users u1 = new Users();
		u1.setUsername("user11");
		u1.setEmail("user11@gmail.com");
		u1=userService.addUser(u1);
		Users u2 = new Users();
		u2.setUsername("user22");
		u2.setEmail("user22@gmail.com");
		u2 =userService.addUser(u2);
		usersRepository.save(u1);
		logger.info("LIst all users: {}", userService.getUsers());
		
		
		Categories c1 = new Categories();
		c1.setName("category 1");
		c1 =blogService.addCategories(c1);
		
		logger.info("categories {}", c1);
		
		Posts p1 = new Posts();
		p1.setBody("post body");
		p1.setTitle("title ");
		
		p1 = postsRepository.save(p1);
		p1.setUsers(u1);
		u1.getPosts().add(p1);
		System.err.println(u1.getPosts());
		u1 = usersRepository.save(u1);
		
		
		
		PostCategories pc1 = new PostCategories();
		pc1.setCategories(c1);
		pc1.setPosts(p1);
		pc1 = postCategoriesRepository.save(pc1);
		
		var posts = blogService.getPosts();
		posts.stream().peek(p -> {
			logger.info("post with id {} and body {} with username {}",p.getId(),p.getBody(),p.getUsers().getUsername());
		}).collect(Collectors.toList());
		
	}

}
