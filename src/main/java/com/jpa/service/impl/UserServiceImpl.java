package com.jpa.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.jpa.entity.Categories;
import com.jpa.entity.PostCategories;
import com.jpa.entity.Posts;
import com.jpa.entity.Users;
import com.jpa.repository.CategoryRepository;
import com.jpa.repository.PostCategoriesRepository;
import com.jpa.repository.PostsRepository;
import com.jpa.repository.UsersRepository;
import com.jpa.service.BlogService;
import com.jpa.service.UserService;

@Service
public class UserServiceImpl implements UserService{
	
	private final UsersRepository usersRepository;
	private final PostsRepository postsRepository;


	public UserServiceImpl(UsersRepository usersRepository, PostsRepository postsRepository) {
		super();
		this.usersRepository = usersRepository;
		this.postsRepository = postsRepository;
	}


	@Override
	public List<Users> getUsers() {
		return usersRepository.findAll();
	}


	@Override
	public Users getUserById(Integer id) {
		return usersRepository.findById(id).orElse(null);
	}


	@Override
	public Users addUser(Users user) {
		return usersRepository.save(user);
	}


	@Override
	public Users updateUser(Integer id,Users user) {
		Users user1 = getUserById(id);
		user1.setUsername(user.getUsername());
		user1.setEmail(user.getEmail());
		user1.setPassword(user.getPassword());
		user1.setDateCreated(user.getDateCreated());
		user1.setDateModified(user.getDateModified());
		return usersRepository.save(user);
	}


	@Override
	public void deleteUser(Integer id) {
		usersRepository.deleteById(id);
		
	}


	@Override
	public List<Posts> getUserPosts(String username) {
		return postsRepository.findAllByUsers_username(username);
	}


	@Override
	public List<Posts> getUserPostsById(Integer userId) {
		return postsRepository.findAllByUsers_id(userId);
	}


	@Override
	public Posts addUserPost(Posts post) {
		return postsRepository.save(post);
	}


	@Override
	public void deleteUserPost(Integer postId) {
		postsRepository.deleteById(postId);
	}

}
